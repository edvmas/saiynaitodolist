<?php


		
	include_once 'header.php';
	if (!empty($_SESSION['user'])) {
	
	include_once 'login.php';
	$queryResult = getData($_SESSION['id']);

?>
<div  id="main" style="max-width:1280px; margin:auto;" >
	<div id= "button_index" class="container col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	
		<!-- The form used to print the table data from the "done" tasks -->
		<div id="table1" class="container col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<ul id="draggablePanelList1" class="list-unstyled">
			<?php $queryResult =  getData($_SESSION['id']);
			while ($queryRow = $queryResult->fetch()){ 
				if($queryRow['Removed']==0 && $queryRow['Done']==1){
				?>
				<li class="panel panel-info">
				<div  class="row hoverDiv">
										<div class="col col-sm-2 col-md-1 col-lg-1 col-xl-1">
						<form method="post" action="/toDoList/task.php">
							<button type='submit' name="unfinishTask" class='btn btn-xs'>
								<img class="check_img" style="height:30px;" src="./style/undo.svg" alt="checklist">
							</button>
							<input type="hidden" name="return" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
							<input type="hidden" name="taskID" value="<?php echo $queryRow['id_todolist']; ?>" />
							<input type="hidden" name="task" value="<?php echo $queryRow['ToDoTask']; ?>" />
							<input type="hidden" name="removed" value="<?php echo $queryRow['Removed']; ?>" />	
						</form>
					</div>
						<div class="col col-sm-2 col-md-2 col-lg-1 col-xl-1">
						<form  method="post" action="/toDoList/task.php">
							<button type='submit' name="removeTask" class='btn btn-xs'>
								<img class="check_img" style="height:30px;" src="./style/x-button.svg" alt="checklist">
							</button>
							<input type="hidden" name="return" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
							<input type="hidden" name="taskID" value="<?php echo $queryRow['id_todolist']; ?>" />
							<input type="hidden" name="task" value="<?php echo $queryRow['ToDoTask']; ?>" />
							<input type="hidden" name="done" value="<?php echo $queryRow['Done']; ?>" />
						</form>
					</div>
					<div class="col col-sm-8 col-md-9 col-lg-10 col-xl-10">
						<h4> <?php echo ($queryRow['ToDoTask']) ?> </h4>
						<h6><?php echo ($queryRow['FinishingTime']) ?><h6>
					</div>
				</div>
				</li>
			<?php
				}
			} ?>
			</ul>
		</div>
	</div>
</div>
<!-- modal form for remove -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="modal-header">
				<h2> Information </h2>
			</div>
			<div class="modal-body">
					<img class="check_img" style="height:30px;" src="./style/x-button.svg" alt="checklist"> <h4> You can move the task to trash </h4>
					<img class="check_img" style="height:30px;" src="./style/undo.svg" alt="checklist"> <h4> You can move the task back to the main list from the done tab </h4>
			</div>
			<div class="modal-footer">
					<button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
  .then(function() { document.getElementById('removeForm').submit(); })
})
jQuery(function($) {
	var panelList = $('#draggablePanelList1');

	panelList.sortable({
		update: function() {
			$('.panel', panelList).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
			});
		}
	});
});
</script>
	<?php
		}
		else {
			header("location: login.php");
		}
	include_once 'footer.php';
?>

