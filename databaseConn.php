<?php

//Establish a connection with the database
function getInput() {
  $arr = array();
  parse_str(file_get_contents("php://input"), $arr);
  foreach ($arr as $key => $value) {
    unset($arr[$key]);
    $arr[str_replace('amp;', '', $key)] = $value;
  }
  return $arr;
}

function database_connect() {

	//Database login data
	$servername = "localhost";
	$username = "root";
	$password = "";
	$database = "task";
	
	// Create connection
	$conn = new PDO("mysql:host=$servername;dbname=$database;charset=utf8", $username);

	// Check the connection
	try {
	$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
	}
	
    return $conn;
}

//Close the database connection
function database_close($conn) {
	$conn=null;
}

?>
