<?php


		
	include_once 'header.php';
	
	if (!empty($_SESSION['user'])) {
	include_once 'login.php';
	$queryResult = getData($_SESSION['id']);

?>



<div  id="main" style="max-width:1280px; margin:auto;" >
	<div id= "button_index" class="container col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div style="background: #CCCCCC;" class="container col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div  id="submit" class="row hoverDiv">
				<form id="form22" action="/toDoList/task.php"  method="post">
					<div class="col col-sm-2 col-md-2 col-lg-2 col-xl-2">
						<h4><label style="align:center;" for="text">New task:</label><h4>
					</div>
					<div class="col col-sm-7 col-md-9 col-lg-9 col-xl-9 input-effect">

					</div>
					<div  id="text" name="task" class="col col-sm-7 col-md-9 col-lg-9 col-xl-9">
						<input class="effect-20" type="text"  name="task" id="text" placeholder="Write a task to add">
						<span class="focus-border">
							<i></i>
						</span>
						<input type="hidden" name="return" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
						<input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>" />
					</div>
					<div class="col col-sm-3 col-md-1 col-lg-1 col-xl-1">
						<button type="submit" name="addTask" class="btn  btn-xs">Submit</button>
					</div>
				</form>
			</div>
		</div>
		
	<!-- The form used to print the table data from the "to do" tasks -->
		<div id="table2" class="container col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<ul id="draggablePanelList" class="list-unstyled">
			<?php while ($queryRow = $queryResult->fetch()){ 
					if($queryRow['Removed']==0 && $queryRow['Done']==0){
					?>
					<li class="panel panel-info">
					<div class="row" >
						<div class="col col-sm-2 col-md-1 col-lg-1 col-xl-1">
							<form method="post" action="/toDoList/task.php">
								<button type='submit' name="finishedTask" class='btn btn-xs'>
									<img class="check_img" style="height:30px;" src="./style/check_icon.svg" alt="checklist">
								</button>
								<input type="hidden" name="return" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
								<input type="hidden" name="taskID" value="<?php echo $queryRow['id_todolist']; ?>" />
								<input type="hidden" name="task" value="<?php echo $queryRow['ToDoTask']; ?>" />
								<input type="hidden" name="removed" value="<?php echo $queryRow['Removed']; ?>" />	
							</form>
						</div>
						<div class="col col-sm-2 col-md-1 col-lg-1 col-xl-1">
							<form id="removeForm" method="post" action="/toDoList/task.php">
								<input type="hidden" name="return" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
								<input type="hidden" name="taskID" value="<?php echo $queryRow['id_todolist']; ?>" />
								<input type="hidden" name="task" value="<?php echo $queryRow['ToDoTask']; ?>" />
								<input type="hidden" name="done" value="<?php echo $queryRow['Done']; ?>" />
								<button type='submit' name="removeTask"  class='btn btn-xs'>
									<img class="check_img" style="height:30px;" src="./style/x-button.svg" alt="checklist">
								</button>
							</form>
						</div>
						<div class="col col-sm-8 col-md-10 col-lg-10 col-xl-10">
							<h4> <?php echo ($queryRow['ToDoTask']) ?> </h4>
							<h6><?php echo ($queryRow['RegisterTime']) ?><h6>
						</div>
					</div>
					</li>
					<?php
					}
				} ?>
			</ul>			
		</div>
	</div>
</div>



<!-- modal form for remove -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="modal-header">
				<h2> Information </h2>
			</div>
			<div class="modal-body">
					<img class="check_img" style="height:30px;" src="./style/check_icon.svg" alt="checklist"> <h4> You can mark tasks as done </h4>
					<img class="check_img" style="height:30px;" src="./style/x-button.svg" alt="checklist"> <h4> You can move the task to trash </h4>
			</div>
			<div class="modal-footer">
					<button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
  .then(function() { document.getElementById('removeForm').submit(); })
})

jQuery(function($) {
	var panelList = $('#draggablePanelList');

	panelList.sortable({
		update: function() {
			$('.panel', panelList).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
			});
		}
	});
});

// JavaScript for label effects only
	$(window).load(function(){
		$(".col-3 input").val("");
		
		$(".input-effect input").focusout(function(){
			if($(this).val() != ""){
				$(this).addClass("has-content");
			}else{
				$(this).removeClass("has-content");
			}
		})
	});
</script>
	<?php
		}
		else {
			header("location: login.php");
		}
	include_once 'footer.php';
?>

