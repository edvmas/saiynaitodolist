

<!DOCTYPE html>
<html>
    <head>
		

	<script src="https://www.gstatic.com/firebasejs/5.7.0/firebase.js"></script>
	<script>
	
	  // Initialize Firebase
	  var config = {
		apiKey: "AIzaSyBxl2UhXPQIZ8xYidAIJliyzXT5dK-YABM",
		authDomain: "todolist-384d8.firebaseapp.com",
		databaseURL: "https://todolist-384d8.firebaseio.com",
		projectId: "todolist-384d8",
		storageBucket: "todolist-384d8.appspot.com",
		messagingSenderId: "77977637129"
	  };
	  firebase.initializeApp(config)
	</script>
	
		<!-- Bootstrap scripts -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Personal to-do list</title>
		<script src="https://code.jquery.com/jquery-2.2.0.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<link href='https://fonts.googleapis.com/css?family=Aclonica' rel='stylesheet'>
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		
		<!-- Fonts-->
		<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=ZCOOL+KuaiLe" rel="stylesheet">
		
		
		<!-- Style files-->
		<link rel="stylesheet" type="text/css" href="style/style.css" title="Default Styles" media="screen">
		<link rel="stylesheet" type="text/css" href="style/styleRegister.css" title="Default Styles" media="screen">
		<link rel="stylesheet" type="text/css" href="style/styleLogin.css" title="Default Styles" media="screen">
		<link rel="stylesheet" type="text/css" href="style/styleHeaderMenu.css" title="Default Styles" media="screen">

    </head>

	
<body>

	<?php
		
		require __DIR__.'/vendor/autoload.php';
		include_once 'databaseConn.php';
		include_once 'function.php';	
		include_once 'session.php';
		
	if (empty($_SESSION['user'])) { } 
	else {
		?>
	

<div class="topnav" id="myTopnav">
  <a href="./index.php">Home</a>
  <a href="./done.php">Done</a>
  <a href="./trash.php">Trash</a>
  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
  	<div class="float-left">
		<button class='btn btn-xs' data-toggle="modal" data-target="#myModal" >
			<img class="check_img" style="height:30px; margin: 10px 10px 10px 10px;   color: #CCCCCC;" src="./style/QUEST.svg" alt="checklist">
		</button>
	</div>

</div>

<script>
    $(function() {
        $('#nav a').click(function() {
           $('#nav a').removeClass();
           $($(this).attr('href')).addClass('active');
        });
     });

function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

</script>
<?php
	}

 /* TODO
1. Header, Content, Footer srityse stilius turėtų būti skirtingas (specifiškas). Srityse turėtų būti bent po keletą skirtingų elementų. 
Analogiški elementai skirtingose srityse turėtų įgauti skirtingą išvaizdą.
2. Padarytas responsive meniu (desktop: horizontalūs punktai, mobile: hamburger)
3.Panaudoti transitions arba animacijas, pagyvinančius naudotojo sąsają (UI) ????
4.Panaudoti vektorines ikonas (webfont, svg).
5.Panaudoti modalinį langą, kuriame pateikiama aktuali/prasminga informacija.
	TODO: 
	2. ideti meniu juosta i header padaryti responsive kad keistusi i hamburher  (plius minus - neveikia aktyvios paryškinimas)
	3. modalinis (issokantis langas) pranesti apie blogai ivestus duomenis.
	4. modalinis langas naikinimo patvirtinimui (plius minus -  panaikina ne ta kuri reikia o pirma is saraso)
	7. prideti kazkokia animacija galbut lenteles atsiradimui?? (fade- modalinio lango maybe tiks)
	   
*/






