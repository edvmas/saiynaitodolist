<?php
	include_once 'databaseConn.php';
	
//Can be used to delete the task from the database by id.
function deleteTask($id){
	$conn = database_connect();
	$query = "DELETE FROM `todolist` WHERE `id_todolist`='$id'";
	$conn->query($query);
	database_close($conn);
}

//Can be used to get all the data needed from the main task table.
function getData($id){
	$id=$_SESSION['id'];
	$conn = database_connect();
	$sql = "select * from todolist WHERE fk_user = $id ;";
	$queryResult = $conn->query($sql);
	$queryResult->setFetchMode(PDO::FETCH_ASSOC);
	return $queryResult;
	database_close($conn);
}

//Can be used to add new tasks to the database.
function addTask($task, $id){
	$conn = database_connect();
	$stmt = $conn->prepare("INSERT INTO `todolist` (`ToDoTask`, `RegisterTime`, `FinishingTime`, `Done`, `Removed`, `fk_user`) 
	VALUES ('$task', localtime(), NULL, '0', '0', '$id');");
	$queryResult = 	$stmt->execute();
	database_close($conn);
}

//Can be used for updating the already existing tasks by id.
function editTask($task, $done, $removed, $id){
	$conn = database_connect();
	$query = "UPDATE `todolist` SET `ToDoTask`='$task', `FinishingTime`=localtime(), 
	`Done`='$done',`Removed`='$removed' WHERE `id_todolist`='$id'";
	$conn->query($query);
	database_close($conn);
}

function login($mail, $pass, $return){
	
	$_SESSION['mail_login'] = $mail;
	$_SESSION['pass_login'] = $pass;
	$_SESSION['prev'] = $return;
	
    $user= checkString($mail);

	list($dbuname, $dbpass, $dblevel, $dbuid, $dbemail) = checkdb($mail);
	if ($dbuname) {  //yra vartotojas DB
	
		$_SESSION['id'] = $dbuid;
		
		if (checkpass($pass, $dbpass)) { 
			$_SESSION['user'] = $user;
			header("location: index.php");
		}
		else{
			   header("location: ".$_POST['return']);
		}
	}
	else {
		   header("location: ".$_POST['return']);
	}
    die();
}

function registerNew($user, $mail, $pass, $captcha, $return){
		$user=checkString($_POST['name']);
			
		$_SESSION['name_login'] = $user;
		$_SESSION['pass_login'] = $pass;
		$_SESSION['mail_login'] = $mail;
		$_SESSION['prev'] = $return;
		
		if (checkname($user)) { 
			list($dbuname) = checkdb($mail);        
			if ($dbuname) { 
				$_SESSION['name_error'] = "* The username is already taken</font>";
			} else {  
				$_SESSION['name_error'] = "";
				
				if ($_SESSION["code"] =! $captcha) {
				 $_SESSION['name_error'] ="Wrong TEXT Entered";
					header("location: ".$return);
				}
				
				if($_POST['pass']!=$_POST['pass2']){
					$_SESSION['name_error'] = "Passwords don't match";
				} else
				if (checkpass($pass, substr(hash('sha256', $pass), 5, 32)) && checkmail($mail)) { 
					$id = md5(uniqid($user));                         
					$pass = substr(hash('sha256', $pass), 5, 32);    
					$ulevel=1;
					$date=date("Y-m-d h:i:sa");

					$conn = database_connect();
					
					//REGISTER FIREBASE
					$result = $conn->query("INSERT INTO user(username, password, userlevel, email, date)
						  VALUES ('$user', '$pass', '$ulevel', '$mail', '$date')");

					if ($result) {
					
						$_SESSION['name_error'] = "Registration was succesful. You can now log in.";
						header("location: index.php");
						   die();
					} else {
						$_SESSION['name_error'] = "DB registracijos klaida:" . $result . "<br>" . mysqli_error($conn);
					}

					database_close($conn);
				}
			}
		}
	header("location: ".$_SESSION['prev']);
	die();
}

function checkdb($mail) {  // iesko DB pagal varda, grazina {vardas,slaptazodis,lygis,id,elpastas} ir nustato name_error

	$link = mysqli_connect("127.0.0.1", "root", "", "task");
    $query = "SELECT * FROM user WHERE email = '$mail'";
	$result = mysqli_query($link, $query);
    $uname = $upass = $ulevel = $uid = $umail = $date = null;
    if (!$result ) {
        $_SESSION['name_error'] = "* No such user exists</font>";
    } else {  //vardas yra DB
        $row = mysqli_fetch_assoc($result);
        $uname = $row["username"];
        $upass = $row["password"];
        $ulevel = $row["userlevel"];
        $uid = $row["user_id"];
        $umail = $row["email"];
    }
    database_close($conn);
    return array($uname, $upass, $ulevel, $uid, $umail);
}

function checkmail($mail) {   // e-mail sintax error checking  
    if (!$mail || strlen($mail = trim($mail)) == 0) {
        $_SESSION['message'] = "* Input email address</font>";
        return false;
    } elseif (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
        $_SESSION['message'] = "* Wrong email address format</font>";
        return false;
    } else
        return true;
}

function checkname($username) {   // Vartotojo vardo sintakse
    if (!$username || strlen($username = trim($username)) == 0) {
        $_SESSION['name_error'] = "* Input username";
        return false;
    } elseif (!preg_match("/^([0-9a-zA-Z])*$/", $username)) /* Check if username is not alphanumeric */ {
        $_SESSION['name_error'] = "* Username should be from<br>
				&nbsp;&nbsp;only numbers and letters</font>";
        return false;
    } else
        return true;
}

function checkpass($pwd, $dbpwd) {
    if (!$pwd || strlen($pwd = trim($pwd)) == 0) {
        $_SESSION['pass_error'] = "* Input password</font>";
        return false;
    } elseif (!preg_match("/^([0-9a-zA-Z])*$/", $pwd)) /* Check if $pass is not alphanumeric */ {
        $_SESSION['pass_error'] = "* Password should be from<br>&nbsp;&nbsp;only letters and numbers";
        return false;
    } elseif (strlen($pwd) < 4) {  // per trumpas
        $_SESSION['pass_error'] = "* Pasword length <4 simbolius</font>";
        return false;
    } elseif ($dbpwd != substr(hash('sha256', $pwd), 5, 32)) {
        $_SESSION['pass_error'] = "* Wrong password or username</font>";
        return false;
    } else
        return true;
}

function checkInt($val){
    if (!preg_match('/^\d*$/', $val)) return -64546;
    return $val;
}
function checkString($val){
    return preg_replace("/[^a-zA-Z0-9]+/", " ", $val);
}

?>

