-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2018 at 03:19 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task`
--

-- --------------------------------------------------------

--
-- Table structure for table `todolist`
--

CREATE TABLE `todolist` (
  `ToDoTask` text COLLATE utf8_lithuanian_ci NOT NULL,
  `RegisterTime` datetime NOT NULL,
  `FinishingTime` datetime DEFAULT NULL,
  `Done` tinyint(1) NOT NULL,
  `Removed` tinyint(1) NOT NULL,
  `id_todolist` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `todolist`
--

INSERT INTO `todolist` (`ToDoTask`, `RegisterTime`, `FinishingTime`, `Done`, `Removed`, `id_todolist`) VALUES
('Testing the system', '2018-09-21 12:50:55', '2018-09-21 12:59:48', 0, 1, 66),
('Find new ideas to add to the database', '2018-09-21 12:51:09', '2018-09-21 16:16:28', 1, 0, 67),
('Do the assignment until Friday evening', '2018-09-21 12:51:23', NULL, 0, 0, 68),
('Send the assignment', '2018-09-21 12:51:27', '0000-00-00 00:00:00', 0, 0, 69),
('Never abandon your goals', '2018-09-21 12:51:43', '2018-09-21 16:15:26', 0, 0, 70),
('Set the limits and do the testing', '2018-09-21 12:51:52', '2018-09-21 16:14:48', 0, 0, 71),
('Create a cool interface', '2018-09-21 12:59:58', '2018-09-21 16:14:59', 0, 0, 72),
('Learn something new from every experience', '2018-09-21 13:00:04', '2018-09-21 16:16:26', 1, 0, 73),
('Loooooooooooooonnnnnggggg looooooooooooong Loooooooooooooonnnnnggggg looooooooooooongLoooooooooooooonnnnnggggg looooooooooooongLoooooooooooooonnnnnggggg looooooooooooongLoooooooooooooonnnnnggggg looooooooooooongLoooooooooooooonnnnnggggg looooooooooooongLoooooooooooooonnnnnggggg looooooooooooong taaaaaaaaaaask', '2018-09-21 16:17:08', '2018-09-21 16:17:41', 0, 1, 74);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `todolist`
--
ALTER TABLE `todolist`
  ADD KEY `Removed` (`id_todolist`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `todolist`
--
ALTER TABLE `todolist`
  MODIFY `id_todolist` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
