<?php


		
	include_once 'header.php';
	
	if (!empty($_SESSION['user'])) {
	
	include_once 'login.php';
	$queryResult = getData($_SESSION['id']);

?>
<div  id="main" style="max-width:1280px; margin:auto;" >
	<div id= "button_index" class="container col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="container col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div  id="submit" class="row hoverDiv">
				<form id="form22" action="/toDoList/task.php"  method="post">
					<div class="col col-sm-2 col-md-2 col-lg-2 col-xl-2">
						<h4><label style="align:center;" for="text">New task:</label><h4>
					</div>
					<div class="col col-sm-7 col-md-9 col-lg-9 col-xl-9">
						<input type="test" class="form-control" name="task" id="text" placeholder="Enter task">
						<input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>" />
					</div>
					<div class="col col-sm-3 col-md-1 col-lg-1 col-xl-1">
						<button type="submit" name="addTask" class="btn  btn-xs">Submit</button>
					</div>
				</form>
			</div>
		</div>

	<!-- The form used to print the table data from the "to do" tasks -->
		<div id="table2" class="container col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<ul id="draggablePanelList" class="list-unstyled">
			<?php while ($queryRow = $queryResult->fetch()){ 
					if($queryRow['Removed']==0 && $queryRow['Done']==0){
					?>
					<li class="panel panel-info">
					<div class="row hoverDiv" >
						<div class="col col-sm-2 col-xl-1">
							<form method="post" action="/toDoList/task.php">
								<button type='submit' name="finishedTask" class='btn btn-xs'>
									<img class="check_img" style="height:30px;" src="./style/check_icon.svg" alt="checklist">
								</button>
								<input type="hidden" name="taskID" value="<?php echo $queryRow['id_todolist']; ?>" />
								<input type="hidden" name="task" value="<?php echo $queryRow['ToDoTask']; ?>" />
								<input type="hidden" name="removed" value="<?php echo $queryRow['Removed']; ?>" />	
							</form>
						</div>
						<div class="col col-sm-2 col-xl-1">
							<form  method="post" action="/toDoList/task.php">
								<button type='submit' name="deleteTask" class='btn btn-xs'>
									<img class="check_img" style="height:30px;" src="./style/x-button.svg" alt="checklist">
								</button>
								<input type="hidden" name="taskID" value="<?php echo $queryRow['id_todolist']; ?>" />
								<input type="hidden" name="task" value="<?php echo $queryRow['ToDoTask']; ?>" />
								<input type="hidden" name="done" value="<?php echo $queryRow['Done']; ?>" />
							</form>
						</div>
						<div class="col col-sm-8 col-xl-10">
							<h4> <?php echo ($queryRow['ToDoTask']) ?> </h4>
							<h6><?php echo ($queryRow['RegisterTime']) ?><h6>
						</div>
					</div>
					</li>
					<?php
					}
				} ?>
			</ul>
		</div>
		
		
		<!-- The form used to print the table data from the "done" tasks -->
		<div id="table1" class="container col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<ul id="draggablePanelList1" class="list-unstyled">
			<?php $queryResult =  getData($_SESSION['id']);
			while ($queryRow = $queryResult->fetch()){ 
				if($queryRow['Removed']==0 && $queryRow['Done']==1){
				?>
				<li class="panel panel-info">
				<div  class="row hoverDiv">
					<div class="col col-md-8 col-lg-8">
						<h4> <?php echo ($queryRow['ToDoTask']) ?> </h4>
						<h6><?php echo ($queryRow['FinishingTime']) ?><h6>
					</div>
						<div class="col col-md-1 col-xl-2">
						<form method="post" action="/toDoList/task.php">
							<button type='submit' name="unfinishTask" class='btn btn-xs'>
								<img class="check_img" style="height:30px;" src="./style/undo.svg" alt="checklist">
							</button>
							<input type="hidden" name="taskID" value="<?php echo $queryRow['id_todolist']; ?>" />
							<input type="hidden" name="task" value="<?php echo $queryRow['ToDoTask']; ?>" />
							<input type="hidden" name="removed" value="<?php echo $queryRow['Removed']; ?>" />	
						</form>
					</div>
						<div class="col col-md-1 col-xl-2">
						<form  method="post" action="/toDoList/task.php">
							<button type='submit' name="deleteTask" class='btn btn-xs'>
								<img class="check_img" style="height:30px;" src="./style/x-button.svg" alt="checklist">
							</button>
							<input type="hidden" name="taskID" value="<?php echo $queryRow['id_todolist']; ?>" />
							<input type="hidden" name="task" value="<?php echo $queryRow['ToDoTask']; ?>" />
							<input type="hidden" name="done" value="<?php echo $queryRow['Done']; ?>" />
						</form>
					</div>
				</div>
				</li>
			<?php
				}
			} ?>
			</ul>
		</div>
	</div>
</div>

	
<script>
jQuery(function($) {
	var panelList = $('#draggablePanelList');

	panelList.sortable({
		update: function() {
			$('.panel', panelList).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
			});
		}
	});
});
jQuery(function($) {
	var panelList = $('#draggablePanelList1');

	panelList.sortable({
		update: function() {
			$('.panel', panelList).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
			});
		}
	});
});
</script>
	<?php
		}
		else {
			header("location: login.php");
		}
	include_once 'footer.php';
?>

