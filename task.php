<?php 

	include_once 'function.php';
	include_once 'header.php';

	//Can use to test if elements are sent correctly from the form after submit. 
	//It can be done by commenting the line at the end "header('location:index.php');" and visiting the site.

		print_r($_POST);
		//print_r($_POST);
	//Checking if the task id was sent
	if(isset($_POST['taskID'])){
		$id = $_POST['taskID'];
	}
	
	//Checking if the task text was sent
	if(isset($_POST['task'])){
		$task = $_POST['task'];
	}
	
	//Checking if the submit we received was for adding new tasks and if we have the needed information.
	if(isset($_POST['addTask']) && isset($task)){
		$id=$_POST['id'];
		addTask($task, $id);
	}

	//Checking if the submit we received was for deleting tasks and if we have the needed information.	
	if(isset($_POST['deleteTask']) && isset($id)){
		//editTask($task, $_POST['done'], 1, $id);
		deleteTask($id);
	}
	
	//Checking if the submit we received was for deleting tasks and if we have the needed information.	
	if(isset($_POST['removeTask']) && isset($id)){
		editTask($task, $_POST['done'], 1, $id);
	}

		
	//Checking if the submit we received was for deleting tasks and if we have the needed information.	
	if(isset($_POST['turnBack']) && isset($id)){
		editTask($task, $_POST['done'], 0, $id);
	}
	
	//Checking if the submit we received was for marking tasks as finished and if we have the needed information.
	if(isset($_POST['finishedTask']) && isset($task)){
		editTask($task, 1, $_POST['removed'], $_POST['taskID']);
	}
	
	//Checking if the submit we received was for marking that task is not finished and if we have the needed information.
	if(isset($_POST['unfinishTask']) && isset($task)){
		editTask($task, 0, $_POST['removed'], $id);
	}
	//Checking if the submit was from registration
	if(isset($_POST['register'])){

		registerNew($_POST['name'], $_POST['mail'], $_POST['pass'], $_POST['captcha'], $_POST['return']);
	}
	
	//Checking if the submit was from log in
	if(isset($_POST['login'])){
		login($_POST['mail'], $_POST['pass'], $_POST['return']);
	}

	header("location: ".$_POST['return']);
?>