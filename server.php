<?php

	//TODO task.php has to call server.php
	//TODO server has to get only 
	include_once 'databaseConn.php';
	include_once 'function.php';
	include_once 'client.php';
	// get the HTTP method, path and body of the request
	$method = $_SERVER['REQUEST_METHOD'];
	 
	// connect to the mysql database
	$link = mysqli_connect('localhost', 'root', '', 'task');
	mysqli_set_charset($link,'utf8');
	
	$email =  VerifyUser();
	$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
		
	// retrieve the table and key from the path
	$name = preg_replace('/[^a-z0-9_]+/i','',array_shift($request));
	$key = array_shift($request)+0;
	$smth = array_shift($request);
	$sql2 = "select MAX(id) AS id from `$name`";
	$index_max = mysqli_query($link,$sql2);
	$sql = "select * from `$name`".($key?" WHERE id_todolist=$key":'');
	$result = mysqli_query($link,$sql);

	
	
	$value = getArray($sql, $link);
	
	if (!$result) {
	  http_response_code(404);
	  die(mysqli_error());
	}

	$email =  VerifyUser();
		
		$userId = 0;
		if($email!=null){
			$sqlEmail = "select * from `user` WHERE email='$email'";
			$currentUser = getArray($sqlEmail, $link);
			//print_r($currentUser );
			$userId=$currentUser['0']['user_id'];
			$sql22 = "select * from `$name` WHERE fk_user='$userId'";
			if (empty($key)){
				$result = mysqli_query($link,$sql22);
			}
		}
	
	if (empty($key)) {
		
		handle_base($method, $name, $result, $key, $index_max, $smth, $userId);
	} else {	
		handle_name($method, $name, $result, $key, $index_max, $smth,  $value, $userId);	
	}

	function handle_base($method, $name, $result, $key, $index_max, $smth, $userId) {
        switch($method) {
			case 'GET':
				display_el($name, $result, $key, $smth);
				break;
			case 'POST':
				 create_el($name, $result, $key, $index_max, $smth, $userId);
				 break;
			default:
				header('HTTP/1.1 405 Method Not Allowed');
				header('Allow: GET, POST');
				break;
		}
    }

	function handle_name($method, $name, $result, $key, $index_max, $smth,  $value, $userId) {
		switch($method) {
			case 'POST':
				create_el($name, $result, $key, $index_max, $smth, $userId);
				break;

			case 'DELETE':
				delete_el($name, $result, $key, $smth);
				break;
		  
			case 'PUT':
				update_el($name, $result, $key, $smth, $value);
				break;
		  
			case 'GET':
				display_el($name, $result, $key, $smth);
				break;

			default:
				header('HTTP/1.1 405 Method Not Allowed');
				header('Allow: GET, PUT, POST, DELETE');
				break;
			}
    }

	//POST
	function create_el($name, $result, $key, $index, $smth, $userId){
       $data = getInput();
        if(isset($smth)){
			http_response_code(400);
		}
		else if (mysqli_num_rows($result)==1) {
            http_response_code(409);
            return;
        }
		else if(mysqli_num_rows($result)==0){
			http_response_code(400);
		}
		else if(!isset($_COOKIE['token']) || !verify($key)){
		http_response_code(401);
			
		}
		else{
			
	   print_r("post");
			addTask($data['task'], $userId);
		}
    }
    //DELETE
	function delete_el($name, $result, $key, $smth) {
		if(isset($smth)){
			http_response_code(400);
		}
		else if(!isset($_COOKIE['token']) || !verify($key)){
			http_response_code(401);	
		}
        else if (mysqli_num_rows($result)!=0) {
			
			print_r("delete");
			deleteTask($key);
            echo $key;
        } else {
            http_response_code(404);
        }
    }
	
	//PUT
	function update_el($name, $result, $key, $smth, $value){
		  $data = getInput();
		if(isset($smth)){
			http_response_code(400);
		}
		else if(!isset($_COOKIE['token']) || !verify($key)){
		http_response_code(401);
			
		}
		else if (mysqli_num_rows($result)!=0) {
			echo $key;
			http_response_code(200);
			print_r("edit");
			editTask($data['task'], $value['0']['Done'], $value['0']['Removed'], $key); 
			
        } else {
            http_response_code(404);
        }
	}
    
	//GET
    function display_el($name, $result, $key, $smth) {
		if(isset($smth)){
			http_response_code(400);
		}
		else if(!isset($_COOKIE['token']) || !verify($key)){
		http_response_code(401);
			
		}
		else if (mysqli_num_rows($result)!=0) {
			print_r("GET");
			result($result);
        } else {
            http_response_code(404);
        }
    }
    
    /**
     * Displays a list of all objects.
     */
    function result($result) {
        header('Content-type: application/json');
        for ($i=0;$i<mysqli_num_rows($result);$i++) {
				echo ($i>0?',':'').json_encode(mysqli_fetch_object($result));
			}
    }
 
	// close mysql connection
	mysqli_close($link);
	
	?>