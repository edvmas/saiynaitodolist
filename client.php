<?php

require __DIR__.'/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

// This assumes that you have placed the Firebase credentials in the same directory
// as this PHP file.
$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/todolist-384d8-firebase-adminsdk-6tnp8-b46683d4ac.json');

$firebase = (new Factory)
  ->withServiceAccount($serviceAccount)
  ->create();
$GLOBALS['$firebase'] = $firebase;

 
function VerifyUser() {
	try{
	$verifiedIdToken = $GLOBALS['$firebase']->getAuth()->verifyIdToken($_COOKIE['token']);
     return  $verifiedIdToken->getClaim('email');
	}
	catch(Exception $ex){
		return false;
	}
 }
 //TODO it has to work when $task_id is zero
  function verify($task_id) {
	try{
		$conn = database_connect();		
		$link = mysqli_connect('localhost', 'root', '', 'task');
		mysqli_set_charset($link,'utf8');
		
		$sql = "SELECT email FROM `user`
		INNER JOIN `todolist` ON   todolist.fk_user = user.user_id && todolist.id_todolist = $task_id;";
		$queryResult = getArray($sql, $link);

		//print_r($sql );
		$verifiedIdToken = $GLOBALS['$firebase']->getAuth()->verifyIdToken($_COOKIE['token']);
		//print_r($queryResult);
		//print_r($verifiedIdToken->getClaim('email'));
		if($task_id==0){ return true; }
		else if($queryResult==null) { 
            http_response_code(404);
			return true;
		}
		else{
			return $verifiedIdToken && $queryResult['0']['email'] === $verifiedIdToken->getClaim('email');
		}
		
	}
	catch(Exception $ex){
			return false;
	}
 } 
 
 
 /* function verify() {
	try{
	$verifiedIdToken = $GLOBALS['$firebase']->getAuth()->verifyIdToken($_COOKIE['token']);
     return $verifiedIdToken && $_COOKIE['firebaseID'] === $verifiedIdToken->getClaim('user_id');
	}
	catch(Exception $ex){
		return false;
	}
 }*/
 	
function getArray($sql, $link) {
    $result = $link->query($sql);    
    $arr = array();
    if ($result){
      while ($row = $result->fetch_assoc())
        array_push($arr, $row);
    }
    return $arr;
}
  