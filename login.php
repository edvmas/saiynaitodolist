<?php


	include_once 'header.php';

	
	if (empty($_SESSION['user'])) {
		?>
		
		
						
<div class="container" >  

	<form id="form12">
		<h1>To Do List</h1>	
		<img id="logo" src="./style/logo_img.png" alt="logo" class="responsive">
	</form>
	<form id="form11" action="task.php" method="POST" >
		<label>Email:</label>
		<input class="form-control" id="mail" type="text" placeholder="E-Mail" name="mail">
		
		<label>Password:</label>
		<input class="form-control" id="pass" type="password" placeholder="Password" name="pass" >
		
		<button id="login" class="btn" type="submit" name="logi" value="logi">Log in</button>
		<input type="hidden" name="login"  value="logi">
		<input type="hidden" name="return" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<a id="register" class="button btn link" href="register.php">Register</a>
	</form> 
</div>	
		<?php
		
		include_once 'footer.php';
	} 
	else{
	?>


<div class="container" >  
	<form id="form12" class="logout" action="logout.php" method="POST" >
		<div>
			<h2> <?php echo $_SESSION['mail_login']; ?></h2>
			<button class="btn" type="submit" name="logout" value="logout">log out</button>
		</div>
	</form> 
</div>	

<?php
	} 
	
	?>
		<script>
				
			
			document.getElementById("form11").addEventListener("submit", login());
			document.getElementById("form11").addEventListener("submit", getToken());
			document.getElementById("form12").onsubmit = function() {logout()};

			function login(){
				firebase.auth().signInWithEmailAndPassword(document.getElementById('mail').value,document.getElementById('pass').value)
				.catch(function(error) {
				
						  var errorCode = error.code;
						  var errorMessage = error.message;
						  console.log(errorMessage);
						  
						});
			}
			
			//Trying to get the token from the database
			function getToken(){
				
				firebase.auth().onAuthStateChanged(function(user) {
					if (user) {
						user.getIdToken().then(function(token) {
							console.log(user.uid);
							document.cookie = "token="+  token; 
							document.cookie = "firebaseID="+ user.uid;
						});
					}
				})
				.catch(function(error) {

					  var errorCode = error.code;
					  var errorMessage = error.message;
					  console.log(errorMessage);
					  
					})
			}
			
			function logout(){
				document.cookie = "token="; 
				document.cookie = "firebaseID=";
				firebase.auth().signOut()
				.then(function() {document.getElementById('form2').submit();
				})
				.catch(function(error) {
					var errorCode = error.code;
					var errorMessage = error.message;
					console.log(errorMessage);
				});
			}
		
		</script>
